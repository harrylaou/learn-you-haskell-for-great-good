module Types_02 where

removeNonUpperCase :: [Char] -> [Char]
removeNonUpperCase st = [ c | c <- st , c `elem` ['A'..'Z']]

addThree :: Int -> Int -> Int -> Int
addThree x y z = x + y + z

factorialInt :: Int -> Int
factorialInt n = product[1 .. n]


factorial :: Integer -> Integer
factorial n = product[1 .. n]

circumference :: Float -> Float
circumference r = 2 * pi * r

circumference' :: Double -> Double
circumference' r = 2 * pi * r

-- fromIntegral :: (Integral a, Num b) => a -> b
