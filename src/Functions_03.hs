module Functions_03 where


lucky :: Int -> String
lucky 7 = "LUCKY NUMBER SEVEN!"
lucky x = "Sorry, you are out of luck, pal!"

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n-1)


addVectors :: (Double,Double) -> (Double,Double) -> (Double,Double)
-- addVectors a b = (fst a + fst b, snd a + snd b)
addVectors (x1,y1) (x2,y2) = (x1 + x2, y1 + y2)


xs = [(1,3),(4,3),(2,4),(5,3),(5,6),(3,1)]
ys = [a+b | (a,b)<- xs]


head' :: [a]-> a
head' [] = error "Can't call head on empty lsit, dummy !"
head' (x:_) = x

tell :: (Show a) => [a] -> String
tell [] = "The list is empty"
tell (x:[]) = "The list has one element : " ++ show x
tell (x:y:[]) = "The list has two elements : " ++ show x ++ " and " ++ show y
tell (x:y:_) = "The list is long. The first two elements are : " ++ show x ++ " and " ++ show y


firstLetter :: String -> String
firstLetter "" = "Empty string, whoops !"
firstLetter all@(x:_) = "The first letter of " ++ all ++ " is " ++ [x]


bmiTell :: Double -> String
bmiTell bmi
    | bmi <= 18.5 = "You are underweight, you emo, you"
    | bmi <= 25.0 = "You are supposedly normal. Pffft, I bet you are ugly."
    | bmi <= 30 = "You are fat! Lose some weight, fatty"
    | otherwise = "You are a whale, congratulations!"

bmiTell' :: Double -> Double -> String
bmiTell' weight height
    | bmi <= 18.5 = "You are underweight, you emo, you"
    | bmi <= 25.0 = "You are supposedly normal. Pffft, I bet you are ugly."
    | bmi <= 30   = "You are fat! Lose some weight, fatty"
    | otherwise   = "You are a whale, congratulations!"
    where bmi = weight / height ^ 2

bmiTell'' :: Double -> Double -> String
bmiTell'' weight height
    | bmi <= 18.5 = "You are underweight, you emo, you"
    | bmi <= 25.0 = "You are supposedly normal. Pffft, I bet you are ugly."
    | bmi <= 30   = "You are fat! Lose some weight, fatty"
    | otherwise   = "You are a whale, congratulations!"
    where bmi = weight / height ^ 2
          (skinny, normal, fat) = (18.5,25.0,30)
          -- skinny = 18.5
          -- normal = 25.0
          -- fat = 30

max' :: (Ord a) => a -> a -> a
max' a b
    |a <= b    = b
    |otherwise = a



myCompare :: (Ord a)=> a -> a -> Ordering
a `myCompare` b
    | a == b    = EQ
    | a < b     = LT
    | otherwise = GT




niceGreeting = "Hallo! So very nice to see you, "
badGreeting = "Pffft. It's you, "
greet :: String -> String
greet "Juan" = niceGreeting ++ "Juan!"
greet "Fernando" = niceGreeting ++ "Fernando!"
greet name = badGreeting ++ name ++ "!"

initials :: String -> String -> String
initials firstName lastName = [f] ++ ". " ++ [l] ++ "."
    where (f:_)  =firstName
          (l:_) = lastName


calcBmis :: [(Double,Double)] -> [Double]
calcBmis xs = [bmi w h | (w,h) <- xs ]
    where bmi weight height= weight / height ^ 2


cylinder :: Double -> Double -> Double
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^ 2
    in sideArea + 2 * topArea


calcBmis' :: [(Double,Double)] -> [Double]
calcBmis' xs =  [bmi | (w,h) <- xs, let bmi = w / h ^ 2 ]


head'' :: [a] -> a
head'' xs = case xs of []    -> error "No head for empty lists"
                       (x:_) -> x

describeList :: [a] -> String
describeList ls = "The list is " ++ case ls of [] -> "empty."
                                               [x] -> "a singleton list"
                                               xs -> "a longer list."


describeList' :: [a] -> String
describeList' ls = "The list is " ++  what ls
    where what []  = "empty."
          what [x] = "a singleton list"
          what xs  = "a longer list."
